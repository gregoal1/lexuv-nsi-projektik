# Lexův NSI projektík 
Jak na nahrání vzorových dat do influxDB mračno. Plus ukázka části toho co je možné dělat pomocí sytému Home Assistant (HA), kromě klasického ovládání žárovek co si každý představí.

## Jak podsunout ukázková data do InfluxDB obláček

- Vytvořte bucket s názvem 'home_assistant'
- Stáhněte si celý tento repozitář
- Pomocí úhledného kódíku [posun.py](https://gitlab.fel.cvut.cz/gregoal1/lexuv-nsi-projektik/-/blob/main/posun.py) upravte vzorová [data](https://gitlab.fel.cvut.cz/gregoal1/lexuv-nsi-projektik/-/tree/main/data) tak aby se časy posunuly do současnosti, pak vám je InfluxDB mráček nesmaže. Kód stačí spustit a jako mávnutím proutku se s trochou štěstí vytvoří soubor out.csv. Jediné co je nutné provést je přidat pomocí textového editoru, dle vlastní preference, na začátek vzniklého souboru následující tři řádky:
```
#group,false,false,false,false,true,true,true,true,true,true
#datatype,string,long,dateTime:RFC3339,double,string,string,string,string,string,string
#default,mean,,,,,,,,,
```
tyto tři řádky umožní bezproblémové nahrání dat.
- Případně je možné získat i data za 30 dní stačí upravit kódík tak aby si jako zdroj vybral soubor 'mesic.csv'.

- Poté již stačí ve webovém rozhraní InfluxDB: Load Data -> Sources -> Flux Annotared CSV -> označit bucket home_assistant a nahrát soubor 'out.csv' s upravenou hlavičkou.
- Pokud se vše povedlo je teď k dispozici spousta dat na hraní :D
- Pro ukázku jak lze data vizualizovat je k dispozici šablona nástěnky [loket_ha.json](https://gitlab.fel.cvut.cz/gregoal1/lexuv-nsi-projektik/-/blob/main/loket_ha.json) kterou je možné naimportovat Dashboards -> Create dashboard -> Import dashboard. Pro její správnou funkčnost je zapotřebí aby se kbelík opravu jmenoval 'home_assistant'.

Výsledkem by mělo být něco následující podoby:
![dashboard](/uploads/59b232bb14f5cd433e41e367cfda1653/dashboard.png)

## Co v těch ukázkových datech vlastně je
Doporučuji data procházet pomocí atributu 'friendly_name'

K dispozici je 
- Napětí, proud, frekvence, výkon a účiník odběru ze sítě. Fx Voltage, Current...
- Tlak vzduchu: Led Hodiny tlak 
- Všemožné teploty: názvy mluví za sebe
- Údaje o spotřebě vody: vodomer, vodomer_rate

Více do ukázkových dat nebylo pro přehlednost zahrnuto, ale člověka až překvapí, jak rychle dokáže HA odepsat SSD disk do věčných lovišť věčným logováním všemožných údajů ze senzorů. 

### Jak to měřit a využít
Čtenáři doporučiji brát následující text se značnou nadsázkou. :)

Nějaké odečítání blikání z elektroměru je sice super ale když vám to rok, co rok týpek odečítající údaje odpojí tak je lepší si to měření spotřeby trošku vylepšit.

<img src="/uploads/af2be30eebc4e28f4ec38786488684d5/IMG_20220508_143958803_HDR.jpg" width="50%" height="50%">

Koupit již hotové sensory je taky možnost, ale nevyrovná se to tomu si je udělat sám, ať už se jedná o obyčejné čidlo, co je napájené ze sítě:

<img src="/uploads/4658dde25371b840c277758585557dc9/IMG_20220508_144531801_HDR.jpg" width="20%" height="20%">

Nebo zneužití bakalářky na vytvoření něčeho sofistikovanějšího, co si místo po WiFi povídá s HA na 433 MHz s modulací LoRa:

<img src="/uploads/814a1421ca66ee5a26cbe906113955ee/IMG_20220515_141007921_HDR.jpg" width="40%" height="40%">

Nebo využít něčeho, co udělal jiný nadšenec třeba pro odečet vodoměru s [pomocí kamery připojené k ESP32](https://github.com/jomjol/AI-on-the-edge-device).

K dispozici pak bude spousta údajů, jejichž přínos je pro obyčejného smrtelníka diskutabilní, ale správný FELák takové údaje rád sleduje no a jakým způsobem, no přece na něčem co se hýbe. Získá tak designový dekorační prvek, co svítí a hýbe  se mu rafička podle toho jak moc se zrovna šetří cennou elektřinou, stačí jen ESP8266 či jiné podobné, na kterých jde rozběhnout [ESPHome](https://esphome.io/) a nějaký ten nápad. No a co s nudnýma hodinama ESPčko do nich a třeba vedle času zobrazit teploty venku, co si sosá Home Assistant z venkovních čidel.

![IMG_20220509_002148693_HDR](/uploads/cd3a4f6182bb62c74db4e3f758541be0/IMG_20220509_002148693_HDR.jpg)

No a pro ty co jim vadí že to v noci lehce svítí tak zase e-ink displej+ESP8266 a "odkroutit" si tím i nějaký ten projektík do školy.

<img src="/uploads/6b4e7b6c9bc5db5df543c4c6d1a51c93/IMG_20220514_125122960_HDR.jpg" width="50%" height="50%">

### Edit 2023
Je tu nový semestr a nějaký ten další semestrální projektít a ruce mé se dostali k flipdot panelům z MHD. (Člověk se zmíní před správnou osobou že po jendom touží a za týden už si to štráduje na dopravní fakultu, někam do kumbálu klubu, že jim tam překáží celá sada z autobusu. Výraz lidí když sme to táhli tramvají stál za tu námahu nezvít to na pražáka autem.) Takže další zobrazovač dat z HA a efektní osvětlení místnosti jako bonus.

<img src="/uploads/92c1a11cb02a72334e715f33c60c404a/flipdot_velky.jpg" width="100%" height="100%">

Zde malý jako teploměr co posílá údaje po MQTT pro HA a zase zpětně je řízen jeho podsvit.

<img src="/uploads/8bca1d2f199393006e7ee5c307f29031/flipdot_maly.jpg" width="50%" height="50%">

Jelikož panely sou normálně ovládány po sběrnici IBIS která vznikla za krále klacka tak došlo k menší (větší) úpravě řídící elektroniky. Tedy zůstaly jen 3to8 registry které řídí jednotlivé body a o zbytek se již pak stará bastl deska nahrazující výstupy z původního MCU. Takže z panelu co uměl jen ibis je panel co si povídá bezdrátem pomocí MQTT.

<img src="/uploads/92103249f6644ed2c01001bb0cdb6173/rizeni.jpg" width="50%" height="50%">

## Jak donutit systém Home Assistant aby ukládal data na InfluxDB mrak. 
Pokud si toto čte nadšenec co doma provozuje HA stačí v configuration.yalm přidat následujích pár řádků a vše by mělo začít fungovat. [viz dokumentace HA](https://www.home-assistant.io/integrations/influxdb/)
```
influxdb:
  api_version: 2
  host: eastus-1.azure.cloud2.influxdata.com či jiná podivná doména na které provozujete InfluxDB mrakoun
  token: supernáhodnýhroznědlouhýřetězecpodobajícísepísmenkovépolívce
  organization: emailcojsemsidelalkdyzmibylo12@superdomena.cz
  bucket: home_assistant
  tags:
    source: HA
  tags_attributes:
    - friendly_name
  default_measurement: units
  include:
    domains:
      - sensor
      - binary_sensor
    entities:
      - weather.home
```

## Perlička na závěr 
Provozovat HA na něčem jako RaspberryPi je sice sranda, ale po čase se čekání na načítání dat nebo kompilace zdrojáků ESPHome omrzí a je třeba přejít na nějaké silnější železo. A pokud si myslíte, že na to doma není místo, tak vždycky se nějaký ten kout najde. Třeba takové schodiště do sklepa je ideální a nehučet to tak si toho ani nikdo nevšimne.

<img src="/uploads/1d1c830d7fac8b767b05551f304d9fec/IMG_20220507_114359577.jpg" width="30%" height="30%">

Toto provedení je sice ukamenováníhodné, ale doba trvání provizória se limitně blíží nekonečnu, jak tomu už bývá.(edit 2023: je tomu rok a vypadá to pořád stejně jen už to odnesly 2 SSD disky tak sem se plácl přes kapsu a konečne pořídil nějaký trošku kvalitnější co by mohl vydžet) Navíc je pak možné provozovat spoustu dalších věcí jako například vlastní [DNS s blokováním reklam](https://pi-hole.net/) či záznam kamerového systému a to vše ve vlastních virtuálních mašinách pod správou [TrueNAS](https://www.truenas.com/), který jako třešničku na dortu poskytne síťové úložiště  s absolutní kontrolu nade vším.

