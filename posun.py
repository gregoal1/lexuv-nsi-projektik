import pandas as pd
from pandas.tseries.offsets import DateOffset
import numpy as np
 
data = pd.read_csv('data/tyden.csv', skiprows=3)  #preskoci ten paskvil co tam prida influxdb
pd.options.display.max_columns = None

data['_time']  = pd.to_datetime(data._time)  #upravi formát timestampu na něco čemu pandas rozumí
nejnovejsi = data['_time'].max()  
dneska = pd.Timestamp.now().tz_localize(tz='UTC')
rozdil = pd.Timestamp.now().tz_localize(tz='UTC') - nejnovejsi
rozdil = rozdil/np.timedelta64(1,'D')
rozdil = round(rozdil,0)  #všemožné fičury pro oblbnutí InfluxDB aby si myslel že data jsou aktuální

data['_time']  = pd.to_datetime(data._time)+ DateOffset(days=rozdil)


data['_time']= pd.to_datetime(data['_time']).dt.strftime('%Y-%m-%dT%H:%M:%SZ')  #udělá z _time zase něco čemu rozumí InfluxDB


data.to_csv('out.csv',index=False)

#pak už jen přidat tři rádky viz hlavicka.txt
